package com.example.financemanager.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.financemanager.models.Category;
import com.example.financemanager.models.CategoryWithDailyExpenses;

import java.util.List;

@Dao
public interface CategoryDao {
    @Query("SELECT * FROM categories")
    List<Category> getAll();

    @Query("SELECT * FROM categories")
    LiveData<List<Category>> getAllLiveData();

    @Query("SELECT * FROM categories WHERE id = :id LIMIT 1")
    Category findById(String id);

    @Query("SELECT categories.id as id, categories.name as categoryName," +
            "daily_expenses.name as name," +
            "sum(daily_expenses.amount) as amount," +
            "daily_expenses.created_at as created_at," +
            "daily_expenses.addedMonth as addedMonth FROM categories " +
            "LEFT JOIN daily_expenses ON daily_expenses.category_id = categories.id " +
            "AND daily_expenses.addedMonth = :addedMonth " +
            "AND daily_expenses.addedYear = :addedYear " +
            "GROUP BY categories.name")
    LiveData<List<CategoryWithDailyExpenses>> getCategoryWithExpensesLiveData(String addedMonth, String addedYear);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Category category);

    @Update
    void update(Category category);

    @Delete
    void delete(Category category);
}
