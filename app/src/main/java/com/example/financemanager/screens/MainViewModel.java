package com.example.financemanager.screens;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.financemanager.App;
import com.example.financemanager.models.Category;
import com.example.financemanager.models.CategoryWithDailyExpenses;
import com.example.financemanager.models.DailyExpense;
import com.example.financemanager.models.Income;
import com.example.financemanager.models.Outlay;

import java.util.List;

public class MainViewModel extends ViewModel {

    protected static LiveData<List<DailyExpense>> dailyExpenseLiveData(String date) {
        if (date != null) {
            return App.getInstance().getDailyExpenseDao().getAllDayLiveData(date);
        }

        Data today = new Data();

        return App.getInstance().getDailyExpenseDao().getAllDayLiveData(today.getFullDate());
    }

    protected static LiveData<List<CategoryWithDailyExpenses>> categoryWithExpensesLiveData(String month, String year) {
        if (month != null && year != null) {
            return App.getInstance().getCategoryDao().getCategoryWithExpensesLiveData(month, year);
        }

        Data date = new Data();

        return App.getInstance().getCategoryDao().getCategoryWithExpensesLiveData(String.valueOf(date.getMonth()), String.valueOf(date.getYear()));
    }

    protected static LiveData<List<DailyExpense>> getExpensesByCategory(int category_id, String month, String year) {
        if (month != null && year != null) {
            return App.getInstance().getDailyExpenseDao().getAllByCategoryLiveData(category_id, month, year);
        }

        Data date = new Data();

        return App.getInstance().getDailyExpenseDao().getAllByCategoryLiveData(category_id, String.valueOf(date.getMonth()), Integer.toString(date.getYear()));
    }

    private LiveData<List<Income>> incomeLiveData = App.getInstance().getIncomeDao().getAllLiveData();
    private LiveData<List<Outlay>> outlayLiveData = App.getInstance().getOutlayDao().getAllLiveData();
    private LiveData<List<Category>> categoryLiveData = App.getInstance().getCategoryDao().getAllLiveData();

    public LiveData<List<Income>> getIncomeLiveData() {
        return incomeLiveData;
    }

    public LiveData<List<DailyExpense>> getDailyExpenseLiveData(String date) {
        return dailyExpenseLiveData(date);
    }

    public LiveData<List<DailyExpense>> getDailyExpenseLiveDataByCategory(int category_id, String month, String year) {
        return getExpensesByCategory(category_id, month, year);
    }

    public LiveData<List<Outlay>> getOutlayLiveData() {
        return outlayLiveData;
    }

    public LiveData<List<Category>> getCategoryLiveData() {
        return categoryLiveData;
    }

    public LiveData<List<CategoryWithDailyExpenses>> getCategoryWithExpensesLiveData(String month, String year) {
        return categoryWithExpensesLiveData(month, year);
    }
}