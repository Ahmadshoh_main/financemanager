package com.example.financemanager.screens.statistic;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SortedList;

import com.example.financemanager.R;
import com.example.financemanager.models.CategoryWithDailyExpenses;
import com.example.financemanager.screens.Data;
import com.example.financemanager.screens.statisticCategoryItems.StatisticCategoryItemsActivity;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.CategoryViewHolder>{

    private SortedList<CategoryWithDailyExpenses> sortedList;

    public Adapter() {
        sortedList = new SortedList<>(CategoryWithDailyExpenses.class, new SortedList.Callback<CategoryWithDailyExpenses>() {
            @Override
            public int compare(CategoryWithDailyExpenses o1, CategoryWithDailyExpenses o2) {
                return (int) (o2.amount-o1.amount);
            }

            @Override
            public void onChanged(int position, int count) {
                notifyItemChanged(position, count);
            }

            @Override
            public boolean areContentsTheSame(CategoryWithDailyExpenses oldItem, CategoryWithDailyExpenses newItem) {
                return oldItem.equals(newItem);
            }

            @Override
            public boolean areItemsTheSame(CategoryWithDailyExpenses item1, CategoryWithDailyExpenses item2) {
                return item1.id == item2.id;
            }

            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeInserted(position, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition, toPosition);
            }
        });
    }

    @NonNull
    @Override
    public Adapter.CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Adapter.CategoryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.statistic_items, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter.CategoryViewHolder holder, int position) {
        holder.bind(sortedList.get(position));
    }

    @Override
    public int getItemCount() {
        return sortedList.size();
    }

    public void setItems(List<CategoryWithDailyExpenses> categoryWithExpenses) {
        sortedList.replaceAll(categoryWithExpenses);
    }

    static class CategoryViewHolder extends RecyclerView.ViewHolder {

        TextView categoryName, statisticSum;
        CategoryWithDailyExpenses categoryWithExpenses;


        public CategoryViewHolder(@NonNull final View itemView) {
            super(itemView);

            categoryName = itemView.findViewById(R.id.categoryName);
            statisticSum = itemView.findViewById(R.id.statisticSum);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    System.out.println(month + '/' + year);
                    StatisticCategoryItemsActivity.start((Activity) itemView.getContext(), categoryWithExpenses.id, categoryWithExpenses.categoryName);
                }
            });
        }

        @SuppressLint("SetTextI18n")
        public void bind(CategoryWithDailyExpenses categoryWithExpenses) {
            this.categoryWithExpenses = categoryWithExpenses;

            categoryName.setText(categoryWithExpenses.categoryName);

            statisticSum.setText(Float.toString(categoryWithExpenses.amount));
        }
    }
}
