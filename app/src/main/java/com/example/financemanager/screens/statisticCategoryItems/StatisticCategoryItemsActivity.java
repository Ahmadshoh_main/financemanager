package com.example.financemanager.screens.statisticCategoryItems;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.financemanager.R;
import com.example.financemanager.models.DailyExpense;
import com.example.financemanager.screens.Data;
import com.example.financemanager.screens.MainViewModel;

import java.util.List;
import java.util.Objects;


import androidx.appcompat.app.AppCompatActivity;

public class StatisticCategoryItemsActivity extends AppCompatActivity{

    private static String categoryName;
    private static int categoryId;

    public static void start(Activity caller, int categoryId, String categoryName) {
        Intent intent = new Intent(caller, StatisticCategoryItemsActivity.class);

        StatisticCategoryItemsActivity.categoryName = categoryName;
        StatisticCategoryItemsActivity.categoryId = categoryId;

        caller.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.statistic_category_items);
        Toolbar toolbar = findViewById(R.id.statisticCategoryItemToolbar);
        setSupportActionBar(toolbar);
        Data date = new Data();

        setTitle(categoryName);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        RecyclerView recyclerView = findViewById(R.id.statisticCategoryItems_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        final Adapter adapter = new Adapter();
        recyclerView.setAdapter(adapter);


        MainViewModel mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        mainViewModel.getDailyExpenseLiveDataByCategory(categoryId, String.valueOf(date.getMonth()), String.valueOf(date.getYear())).observe(this, new Observer<List<DailyExpense>>() {
            @Override
            public void onChanged(List<DailyExpense> dailyExpenseList) {
                adapter.setItems(dailyExpenseList);
            }
        });
    }
}
