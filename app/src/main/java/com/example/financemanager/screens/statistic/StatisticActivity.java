package com.example.financemanager.screens.statistic;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.financemanager.R;
import com.example.financemanager.models.CategoryWithDailyExpenses;
import com.example.financemanager.screens.Data;
import com.example.financemanager.screens.MainViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;
import java.util.Objects;

public class StatisticActivity extends AppCompatActivity {

    public static void start(Activity caller) {
        Intent intent = new Intent(caller, StatisticActivity.class);

        caller.startActivity(intent);
    }

    private String selectedYear;
    private String selectedMonth;
    public Data date = new Data();
    DatePickerDialog.OnDateSetListener setListener;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_statistic);
        Toolbar toolbar = findViewById(R.id.statisticToolbar);
        setSupportActionBar(toolbar);
        setTitle("Статистика");
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.statistic_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        final SharedViewModel model = new SharedViewModel();
        final Adapter adapter = new Adapter();
        recyclerView.setAdapter(adapter);

        FloatingActionButton calendarButton = (FloatingActionButton) findViewById(R.id.statisticCalendarButton);
        calendarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MonthPickerDialog pd = new MonthPickerDialog();
                pd.setListener(setListener);
                pd.show(getSupportFragmentManager(), "Choose the month");
            }
        });

        setListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                selectedYear = String.valueOf(year);
                selectedMonth = String.valueOf(month);

                mainViewModel(adapter, selectedMonth, selectedYear);
            }
        };

        mainViewModel(adapter, model.getMonth().getValue(), model.getYear().getValue());
    }

    protected void mainViewModel(final Adapter adapter, final String month, final String year) {
        MainViewModel mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        mainViewModel.getCategoryWithExpensesLiveData(month, year).observe(this, new Observer<List<CategoryWithDailyExpenses>>() {
            @Override
            public void onChanged(List<CategoryWithDailyExpenses> categoryWithExpenses) {
                adapter.setItems(categoryWithExpenses);
            }
        });
    }
}
