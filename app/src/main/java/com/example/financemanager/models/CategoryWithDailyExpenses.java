package com.example.financemanager.models;

public class CategoryWithDailyExpenses {

    public int id;
    public String categoryName;
    public String name;
    public float amount;
    public String created_at;
    public String addedMonth;
}